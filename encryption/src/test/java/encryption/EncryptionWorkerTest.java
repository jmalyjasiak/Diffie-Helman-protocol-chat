package encryption;

import encryption.model.Encryption;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EncryptionWorkerTest {

     String toEncrypt;

     @Nested
     @DisplayName("Given string to encrypt ")
     class Encrypt {

          Encryption encryption;
          long key;

          @Nested
          @DisplayName("and base64 encode")
          class Base64Encode {

               @Nested
               @DisplayName(" using cesar")
               class Cesar {

                    @BeforeEach
                    public void setUp() {
                         encryption = Encryption.Cesar;
                         toEncrypt = "sampleString";
                         key = 12;
                    }

                    @Test
                    @DisplayName("should match original after decryption")
                    void shouldMatchOriginal() {
                         String encrypted = EncryptionWorker.ofPayload(toEncrypt)
                                                            .encrypt(encryption, key)
                                                            .base64Encode()
                                                            .getPayload();

                         String decrypted = EncryptionWorker.ofPayload(encrypted)
                                                            .base64Decode()
                                                            .decrypt(encryption, key)
                                                            .getPayload();

                         assertEquals(toEncrypt, decrypted);
                    }

               }

               @Nested
               @DisplayName(" using otp")
               class Otp {

                    @BeforeEach
                    public void setUp() {
                         encryption = Encryption.XOR;
                         toEncrypt = "sampleString";
                         key = 12;
                    }

                    @Test
                    @DisplayName("should match original after decryption")
                    void shouldMatchOriginal() {
                         String encrypted = EncryptionWorker.ofPayload(toEncrypt)
                                                            .encrypt(encryption, key)
                                                            .base64Encode()
                                                            .getPayload();

                         String decrypted = EncryptionWorker.ofPayload(encrypted)
                                                            .base64Decode()
                                                            .decrypt(encryption, key)
                                                            .getPayload();

                         assertEquals(toEncrypt, decrypted);
                    }
               }

               @Nested
               @DisplayName(" using None")
               class None {

                    @BeforeEach
                    public void setUp() {
                         encryption = Encryption.None;
                         toEncrypt = "abcdefghijklmno";
                         key = 12;
                    }

                    @Test
                    @DisplayName("should match original after decryption")
                    void shouldMatchOriginal() {
                         System.out.println("Before encryption: " + toEncrypt);
                         String encrypted = EncryptionWorker.ofPayload(toEncrypt)
                                                            .encrypt(encryption, key)
                                                            .base64Encode()
                                                            .getPayload();
                         System.out.println("After encryption: " + encrypted);

                         String decrypted = EncryptionWorker.ofPayload(encrypted)
                                                            .base64Decode()
                                                            .decrypt(encryption, key)
                                                            .getPayload();

                         System.out.println("After decryption: " + decrypted);
                         assertEquals(toEncrypt, decrypted);
                    }
               }
          }
     }
}