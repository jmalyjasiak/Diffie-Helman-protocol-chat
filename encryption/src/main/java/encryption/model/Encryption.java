package encryption.model;

import encryption.common.EncryptionNames;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;


@AllArgsConstructor
@Getter
public enum Encryption {

     None(EncryptionNames.ENCRYPTION_NONE_VALUE),
     Cesar(EncryptionNames.ENCRYPTION_CESAR_VALUE),
     XOR(EncryptionNames.ENCRYPTION_XOR_VALUE);

     private final String name;

     private static final Map<String, Encryption> map;
     static {
          map = new HashMap<>();
          for (Encryption encryption : Encryption.values()) {
               map.put(encryption.name, encryption);
          }
     }

     public static Encryption findByName(String name) {
          return map.get(name);
     }
}
