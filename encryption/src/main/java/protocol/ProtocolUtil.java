package protocol;

import lombok.extern.slf4j.Slf4j;
import protocol.model.ProtocolParams;

import java.math.BigInteger;

@Slf4j
public class ProtocolUtil {

     public static long computePublicKey(ProtocolParams protocolParams) {
          BigInteger g = BigInteger.valueOf(protocolParams.getG());
          BigInteger privateKey = BigInteger.valueOf(protocolParams.getPrivateKey());
          BigInteger p = BigInteger.valueOf(protocolParams.getP());
          BigInteger publicKey = g.modPow(privateKey, p);
          return publicKey.longValue();
     }

     public static long computeCommonEncKey(ProtocolParams protocolParams) {
          BigInteger receivedKey = BigInteger.valueOf(protocolParams.getReceivedKey());
          BigInteger privateKey = BigInteger.valueOf(protocolParams.getPrivateKey());
          BigInteger p = BigInteger.valueOf(protocolParams.getP());
          BigInteger s = receivedKey.modPow(privateKey, p);
          log.info("Computed common enc key: " + s);
          return s.longValue();
     }
}
