package protocol.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AParameterDTO {

     private Long a;
}
