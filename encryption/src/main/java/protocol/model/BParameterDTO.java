package protocol.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class BParameterDTO {

     private Long b;
}
