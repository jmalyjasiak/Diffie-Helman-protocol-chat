import chat.server.ServerCLIRunner;

public class ServerApp {

     public static void main(String[] args) {
          ServerCLIRunner.run();
     }
}
