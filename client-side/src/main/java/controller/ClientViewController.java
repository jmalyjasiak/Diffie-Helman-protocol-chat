package controller;

import chat.Client;
import chat.ConnectionParameters;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.javaws.exceptions.InvalidArgumentException;
import encryption.model.Encryption;
import io.vavr.control.Try;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import lombok.extern.slf4j.Slf4j;

import java.util.Observable;
import java.util.Observer;

@Slf4j
public class ClientViewController implements Observer {

     private Client client;

     @FXML
     private Button buttonSend;
     @FXML
     private Button buttonConnect;
     @FXML
     private TextField hostTextField;
     @FXML
     private TextField portTextField;
     @FXML
     private TextField usernameTextField;
     @FXML
     private ComboBox encryptionComboBox;
     @FXML
     private TextArea chatTextArea;
     @FXML
     private TextArea messageTextArea;


     public void buttonConnectOnMouseClicked(MouseEvent mouseEvent) throws Exception {
          if(client != null && client.isConnected()) {
               changeEncryption();
          } else {
               initializeConnection();
          }
     }

     private void changeEncryption() throws JsonProcessingException {
          client.changeEncryption(getSelectedEncryption());
     }

     private void initializeConnection() throws InvalidArgumentException {
          ConnectionParameters connectionParameters = getConnectionParameters();
          validateConnectionParameters(connectionParameters);
          client = Client.of(connectionParameters);
          client.addObserver(this);
          Try.run(client::connect)
             .andThen(this::disableConnectSection)
             .andThen(this::enableSendingMessage)
             .onFailure(throwable -> guiLog("Failed to connect: " + throwable.getMessage()));
     }

     private void validateConnectionParameters(ConnectionParameters cp) throws InvalidArgumentException {
          StringBuilder sb = new StringBuilder();
          if(cp.getEncryption() == null) {
               sb.append("Encryption cannot be null!\n");
          }
          if (cp.getHost() == null) {
               sb.append("Host cannot be null!\n");
          }
          if (cp.getPort() == null) {
               sb.append("Port cannot be null!\n");
          }
          if (cp.getUsername() == null) {
               sb.append("Username cannot be null!\n");
          }
          handleValidationResult(sb);
     }

     private void handleValidationResult(StringBuilder sb) throws InvalidArgumentException {
          if(sb.length() != 0) {
               String validationMessage = sb.toString();
               messageTextArea.appendText(validationMessage);
               throw new InvalidArgumentException(new String[]{validationMessage});
          }
     }

     private void enableSendingMessage() {
          buttonSend.setDisable(false);
          messageTextArea.setDisable(false);
     }

     private void disableConnectSection() {
          buttonConnect.setText("Change\nencryption");
          portTextField.setDisable(true);
          usernameTextField.setDisable(true);
          hostTextField.setDisable(true);
     }

     private void guiLog(String s) {
          this.chatTextArea.appendText(s);
     }

     public void buttonSendOnMouseClicked(MouseEvent mouseEvent) throws JsonProcessingException {
          String text = messageTextArea.getText();
          messageTextArea.clear();
          client.encryptAndSendMessage(text);
     }

     public ConnectionParameters getConnectionParameters() {
          return ConnectionParameters.builder()
                                     .host(hostTextField.getText())
                                     .port(Integer.parseInt(portTextField.getText()))
                                     .username(usernameTextField.getText())
                                     .encryption(getSelectedEncryption())
                                     .build();
     }

     private Encryption getSelectedEncryption() {
          return Enum.valueOf(Encryption.class, encryptionComboBox.getSelectionModel().getSelectedItem().toString());
     }

     @Override
     public void update(Observable o, Object arg) {
          log.info((String) arg);
          chatTextArea.appendText(arg.toString() + "\r\n");
     }
}
