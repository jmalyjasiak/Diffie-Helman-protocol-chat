package chat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import encryption.EncryptionWorker;
import encryption.common.RandomGenerator;
import encryption.model.Encryption;
import encryption.model.EncryptionDTO;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import protocol.model.ProtocolParams;
import protocol.ProtocolUtil;
import protocol.exception.ProtocolInitFailure;
import protocol.model.*;
import util.JsonMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Observable;

@Slf4j
public class Client extends Observable {

     private static final String NEWLINE = "\r\n";
     private static ObjectMapper objectMapper = JsonMapper.getInstance().getObjectMapper();

     private final ConnectionParameters connectionParameters;
     private ProtocolParams protocolParameters;
     private Socket socket;
     private OutputStreamWriter output;
     private BufferedReader input;


     public Client(ConnectionParameters connectionParameters) {
          this.connectionParameters = connectionParameters;
          protocolParameters = new ProtocolParams();
     }

     public static Client of(ConnectionParameters connectionParameters) {
          return new Client(connectionParameters);
     }

     private void logAndNotifyObservers(String message) {
          log.info("New event @ Client: " + message);
          super.setChanged();
          super.notifyObservers(message);
     }

     public boolean isConnected() {
          return socket != null;
     }

     public void connect() throws IOException {
          log.info("Attempting connection with: " + connectionParameters.toString());
          socket = new Socket(connectionParameters.getHost(), connectionParameters.getPort());
          input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
          output = new OutputStreamWriter(socket.getOutputStream());
          initializeProtocol();
          startListeningForMessages();
     }

     private void initializeProtocol() throws IOException {
          sendKeysRequest();
          log.info("Waiting for pg params");
          String pgParams = input.readLine();
          listenForBParam();
          PGParametersDTO pgParametersDTO = objectMapper.readValue(pgParams, PGParametersDTO.class);
          protocolParameters.setPG(pgParametersDTO);
          generateAndSendAParam();
          computeSParameter();
          sendEncryptionRequest();
     }

     private void sendKeysRequest() throws JsonProcessingException {
          ClientRequestDTO keysRequest = ClientRequestDTO.builder()
                                                         .request(Common.KEYS_REQUEST_VALUE)
                                                         .build();
          send(keysRequest);
          log.info("Sending keys request");
     }

     private void listenForBParam() {
          log.info("Waiting for B param");

          Thread thread = new Thread(() -> Try.run(this::readBParam)
                                              .onFailure(throwable -> log.error(throwable.toString())));
          thread.setDaemon(true);
          thread.start();
     }

     private void generateAndSendAParam() throws JsonProcessingException {
          long privateKey = RandomGenerator.generatePositiveLong();
          protocolParameters.setPrivateKey(privateKey);
          long publicKey = ProtocolUtil.computePublicKey(protocolParameters);
          protocolParameters.setPublicKey(publicKey);
          AParameterDTO aParameterDTO = AParameterDTO.builder()
                                                     .a(publicKey)
                                                     .build();
          send(aParameterDTO);
          log.info("A parameter send");
     }

     private void computeSParameter() {
          long commonEncKey = ProtocolUtil.computeCommonEncKey(protocolParameters);
          protocolParameters.setCommonEncKey(commonEncKey);
     }

     private void sendEncryptionRequest() throws JsonProcessingException {
          Encryption encryption = connectionParameters.getEncryption();
          EncryptionDTO encryptionDTO = EncryptionDTO.builder()
                                                     .encryption(encryption.getName())
                                                     .build();

          send(encryptionDTO);
          log.info("Encryption request sent");
     }

     private void readBParam() throws IOException {
          String bParam = input.readLine();
          BParameterDTO bParameterDTO = objectMapper.readValue(bParam, BParameterDTO.class);
          protocolParameters.setReceivedKey(bParameterDTO.getB());
     }

     private void startListeningForMessages() throws IOException {
          Thread thread = new Thread(() -> {
               log.info("Started listening");
               String line;
               while ((line = Try.of(input::readLine).getOrNull()) != null) {
                    readMessage(line);
               }
               logAndNotifyObservers("Listening loop ended");
          });
          thread.setDaemon(true);
          thread.start();
     }

     private void readMessage(String line) {
          Try.of(() ->objectMapper.readValue(line, MessageDTO.class))
             .mapTry(this::decryptMessage)
             .map(MessageDTO::toPrettyString)
             .andThen(this::logAndNotifyObservers)
             .onFailure(throwable -> log.error(throwable.toString()));
     }

     private MessageDTO decryptMessage(MessageDTO msg) {
          String decryptedMsg =
                  EncryptionWorker.ofPayload(msg.getMsg())
                                  .base64Decode()
                                  .decrypt(
                                          connectionParameters.getEncryption(),
                                          protocolParameters.getCommonEncKey())
                                  .getPayload();

          return MessageDTO.builder()
                           .from(msg.getFrom())
                           .msg(decryptedMsg)
                           .build();
     }

     public void encryptAndSendMessage(String msg) throws JsonProcessingException {

          if(!protocolParameters.isReadyToForSending()) {
               throw new ProtocolInitFailure("Parameters incomplete to send message");
          }

          String encryptedMessage =
                  EncryptionWorker.ofPayload(msg)
                                  .encrypt(
                                          connectionParameters.getEncryption(),
                                          protocolParameters.getCommonEncKey())
                                  .base64Encode()
                                  .getPayload();

          MessageDTO payload = MessageDTO.builder()
                                         .msg(encryptedMessage)
                                         .from(connectionParameters.getUsername())
                                         .build();
          send(payload);
     }

     private void send(Object payload) throws JsonProcessingException {
          String jsonPayload = objectMapper.writeValueAsString(payload);
          send(jsonPayload);
     }

     private void send(String payload) {
          if (socket != null) {
               Try.run(() -> writeToOutput(payload))
                  .onFailure(throwable -> logAndNotifyObservers(throwable.toString()));
          }
     }

     private void writeToOutput(String msg) throws IOException {
          output.write(msg + NEWLINE);
          output.flush();
          log.info("Message sent");
     }

     public void changeEncryption(Encryption selectedEncryption) throws JsonProcessingException {
          synchronized (this) {
               connectionParameters.setEncryption(selectedEncryption);
               this.sendEncryptionRequest();
          }
     }
}
